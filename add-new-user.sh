#!/bin/bash

[ -f /home/${1}/.bashrc ] && exit 0

useradd ${1} -m -s /bin/bash -g users -G adm,dialout,cdrom,floppy,audio,dip,video,plugdev,netdev,admin

passwd shaun << EOF
$2
$2
EOF

touch /root/new-user-added
