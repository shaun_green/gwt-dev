#!/bin/bash

[ -f /root/base-software-installed ] && exit 0

apt-get install -y ubuntu-desktop xrdp openjdk-7-jre-headless

touch /root/base-software-installed
[ -f /var/run/reboot-required ] && reboot
