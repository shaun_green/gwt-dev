#!/bin/bash

[ -f ~/eclipse-installed ] && exit 0

cd ~
wget http://mirror.aarnet.edu.au/pub/eclipse/eclipse/downloads/drops4/R-4.3.1-201309111000/eclipse-SDK-4.3.1-linux-gtk-x86_64.tar.gz
tar -xvzpf eclipse-SDK-4.3.1-linux-gtk-x86_64.tar.gz

touch ~/eclipse-installed
